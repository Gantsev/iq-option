<?php declare(strict_types=1);


namespace Core\Base\Storage;

use Core\Base\Interfaces\CacheStorageInterface;
use Core\Base\Interfaces\ResourceInterface;

class RedisStorage implements CacheStorageInterface
{
    /** @var \Redis */
    private $redis;

    function __construct(ResourceInterface $redis)
    {
        $this->redis = $redis;
    }

    public function getRandomItems(string $sKey, int $itemsCount): array
    {
        return (array)$this->redis->sRandMember($sKey, $itemsCount);
    }

    public function pushRandomItem(string $sKey, $itemData): bool
    {
        return (bool)$this->redis->sAdd($sKey, $itemData);
    }

    public function setExpire(string $key, int $ttl): bool
    {
        return (bool)$this->redis->expire($key, $ttl);
    }
}
