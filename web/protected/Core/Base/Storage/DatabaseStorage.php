<?php declare(strict_types=1);


namespace Core\Base\Storage;

use Core\Base\Interfaces\PersistentStorageInterface;
use Core\Base\Interfaces\ResourceInterface;

class DatabaseStorage implements PersistentStorageInterface
{
    /** @var \PDO */
    private $pdo;

    function __construct(ResourceInterface $resource)
    {
        $this->pdo = $resource;
    }

    public function pdo()
    {
        return $this->pdo;
    }
}
