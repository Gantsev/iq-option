<?php declare(strict_types=1);

namespace Core\Base;

use Core\Helpers\Config;

/**
 * Class View
 *
 * @package Core\Base
 */
class View
{
    /** @var string */
    private $template;
    /** @var array */
    private $data;
    /** @var \Twig_Environment */
    private $twig;

    /**
     * View constructor
     *
     * @param string $template
     * @param array  $data
     */
    public function __construct(string $template, array $data = [])
    {
        $this->template = $template;
        $this->data = $data;

        $this->twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem(sprintf('%s/Views', APP_PATH)),
            [
                'debug' => true,
            ]
        );
        $this->twig->addExtension(new \Twig_Extension_Debug);

        $config = new Config();

        $this->twig->addFunction(new \Twig_SimpleFunction(
            'getProductImage',
            function ($id) use ($config): string {
                $filename = sprintf('%s/%010d.jpg', $config->get('images.products_path'), $id);

                return file_exists(sprintf('%s%s', WWW_PATH, $filename))
                    ? $filename
                    : sprintf('%s/no_image.jpg', $config->get('images.products_path'));
            }
        ));
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->twig->render(
            $this->template . '.twig',
            $this->data
        );
    }
}
