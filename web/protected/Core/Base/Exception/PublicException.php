<?php declare(strict_types=1);


namespace Core\Base\Exception;

/**
 * Class PublicException
 *
 * @package Core\Base\Exception
 */
class PublicException extends \Exception
{

}
