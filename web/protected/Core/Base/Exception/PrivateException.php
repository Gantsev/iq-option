<?php declare(strict_types=1);


namespace Core\Base\Exception;

/**
 * Class PrivateException
 *
 * @package Core\Base\Exception
 */
class PrivateException extends \Exception
{
}
