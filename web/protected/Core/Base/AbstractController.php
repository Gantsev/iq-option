<?php declare(strict_types=1);

namespace Core\Base;

/**
 * Class Controller
 *
 * @package Core\Base
 */
abstract class AbstractController
{
    protected const JSON_CODE_OK = 200;

    /**
     * Return HTML view
     *
     * @param string $template
     * @param array  $data
     *
     * @return string
     */
    protected function view(string $template, array $data): string
    {
        return (new View($template, $data))->render();
    }

    protected function json(array $data, int $code = self::JSON_CODE_OK)
    {
        return json_encode([
            'code' => $code,
            'data' => $data,
        ], JSON_BIGINT_AS_STRING | JSON_UNESCAPED_UNICODE);
    }
}
