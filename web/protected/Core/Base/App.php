<?php declare(strict_types=1);

namespace Core\Base;

use Core\Base\Exception\PrivateException;

/**
 * Class App
 *
 * @package Core\Base
 */
class App
{
    /**
     * Точка входа
     *
     * @param Router $router
     *
     * @throws PrivateException
     *
     * @access public
     */
    public static function run(Router $router)
    {
        $route = $router->get();
        $controller = sprintf('App\Controllers\%s', $route['controller']);
        if (class_exists($controller)) {
            if (is_callable([$controller, $route['method']])) {
                if (!is_null($route['arg'])) {
                    call_user_func([(new $controller), $route['method']], $route['arg']);
                } else {
                    call_user_func([(new $controller), $route['method']]);
                }
            } else {
                throw new PrivateException('Method is not callable: ' . $route['request']);
            }
        } else {
            throw new PrivateException('Controller is not exists: ' . $controller);
        }
    }
}
