<?php declare(strict_types=1);

namespace Core\Base\Resources;

use Core\Base\Interfaces\ResourceInterface;

class RedisResource extends \Redis implements ResourceInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->connect($_ENV['REDIS_HOST'], (int)$_ENV['REDIS_PORT']);
        $this->setOption(\Redis::OPT_SERIALIZER, (string)\Redis::SERIALIZER_PHP);
    }
}
