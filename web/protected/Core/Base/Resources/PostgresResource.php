<?php declare(strict_types=1);

namespace Core\Base\Resources;

use Core\Base\Interfaces\ResourceInterface;
use Core\Helpers\Config;

class PostgresResource extends \PDO implements ResourceInterface
{
    public function __construct(Config $config)
    {
        parent::__construct(sprintf(
            'pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s',
            $_ENV['DATABASE_HOST'],
            $_ENV['DATABASE_PORT'],
            $_ENV['DATABASE_NAME'],
            $_ENV['db.user'],
            $_ENV['db.password']
        ));

        $this->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
        $this->setAttribute(\PDO::ATTR_ORACLE_NULLS, \PDO::NULL_NATURAL);
        $this->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);
        $this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
}
