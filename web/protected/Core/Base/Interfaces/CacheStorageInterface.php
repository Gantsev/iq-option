<?php declare(strict_types=1);


namespace Core\Base\Interfaces;


interface CacheStorageInterface
{
    public function getRandomItems(string $sKey, int $itemsCount);

    public function pushRandomItem(string $sKey, $itemData): bool;

    public function setExpire(string $key, int $ttl): bool;
}
