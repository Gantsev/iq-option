<?php declare(strict_types=1);


namespace Core\Base\Interfaces;


interface PersistentStorageInterface
{
    public function pdo(): \PDO;
}
