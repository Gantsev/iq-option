<?php declare(strict_types=1);

namespace Core\Base;

use Core\Base\Exception\PublicException;
use Core\Helpers\Http;

/**
 * Class Router
 *
 * @package Core
 */
class Router
{
    const DEFAULT_ROUTE = 'Main/index';

    /** @var array */
    private static $current = [
        'request'    => null,
        'controller' => null,
        'method'     => null,
        'arg'        => null,
    ];

    /** @var array */
    private $routes = [];

    /**
     * @return mixed
     * @access public
     */
    public static function getMethod(): ?string
    {
        return self::$current['method'];
    }

    /**
     * @return mixed
     * @access public
     */
    public static function getController(): ?string
    {
        return self::$current['controller'];
    }

    /**
     * @return mixed
     * @access public
     */
    public static function getRequest(): ?string
    {
        return self::$current['request'];
    }

    /**
     * Добавляет маршрут
     *
     * @param string $url
     * @param string $route
     *
     * @return Router
     *
     * @throws PublicException
     *
     * @access public
     */
    public function add(string $url, string $route): Router
    {
        if (preg_match('~^[a-z]+/[a-z_]+((/:any)|(/:id))*$~i', $route)) {
            $this->routes[$url] = $route;
        } else {
            throw new PublicException('Invalid route: ' . $route);
        }

        return $this;
    }

    /**
     * Возвращает текущий маршрут
     *
     * @return array
     *
     * @access public
     */
    public function get(): array
    {
        $this->prepare();

        return self::$current;
    }

    /**
     * Определяет текущий маршрут по url
     *
     * @access private
     */
    private function prepare(): void
    {
        $path = trim(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL), '/');

        // Есть путь
        if ($path) {
            // Путь полностью совпадает с маршрутом
            if (isset($this->routes[$path])) {
                $this->setCurrent($path, $this->routes[$path], null);
            } else {
                // Путь не найден, пробуем найти по маске
                $RouteIterator = new \ArrayIterator($this->routes);
                while ($RouteIterator->valid()) {
                    $route = str_replace([':any', ':id'], ['([a-z0-9_\-]+)', '([0-9]+)'], $RouteIterator->key());
                    // Проверяем по регулярке
                    if (preg_match(sprintf('~^%s$~i', $route), $path, $params)) {
                        // Путь найден по маске
                        $this->setCurrent($path, $RouteIterator->current(), $params[1]);
                        break;
                    }
                    // След. роут
                    $RouteIterator->next();
                }
            }
        } else {
            $this->setCurrent($path, self::DEFAULT_ROUTE, null);
        }

        // Роут по умолчанию
        if (is_null(self::$current['controller']) || is_null(self::$current['method'])) {
            Http::show404();
        }
    }

    /**
     * Устанавливает текущий маршрут
     *
     * @param string          $request
     * @param string          $route
     * @param string|int|null $arg
     *
     * @access private
     */
    private function setCurrent(string $request, string $route, $arg): void
    {
        preg_match('~^(?P<controller>[a-z]+)/(?P<method>[a-z]+)~i', $route, $split);

        self::$current = [
            'request'    => $request,
            'controller' => $split['controller'],
            'method'     => $split['method'],
            'arg'        => $arg,
        ];
    }
}
