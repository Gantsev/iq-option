<?php declare(strict_types=1);

namespace Core\Helpers;

/**
 * Class Crypt
 *
 * @package Core\Helpers
 *
 * Класс для шифрования всяких данных, чтобы не стырили с базы
 */
class Crypt
{
    /** Ключ шифрования */
    private const KEY = 'ab86d144e3f080b61c7c2e43';

    /** Метод шифрования */
    private const METHOD = 'AES-128-CBC';

    /** Алгоритм шифрования */
    private const ALGORITHM = 'sha256';

    /** Длина ключа для обрезки */
    private const SHA2_LENGTH = 32;

    /**
     * Длина вектора
     *
     * @var int
     */
    private $ivLength = 0;

    /**
     * Crypt constructor.
     */
    public function __construct()
    {
        $this->ivLength = openssl_cipher_iv_length(self::METHOD);
    }

    /**
     * Зашифровать строку
     *
     * @param string $data
     *
     * @return string
     */
    public function encrypt(string $data): string
    {
        $iv = openssl_random_pseudo_bytes($this->ivLength);
        $raw = openssl_encrypt($data, self::METHOD, self::KEY, OPENSSL_RAW_DATA, $iv);

        return base64_encode($iv . hash_hmac(self::ALGORITHM, $raw, self::KEY, true) . $raw);
    }

    /**
     * Расшифровать обратно
     *
     * @param string $hash
     *
     * @return null|string
     */
    public function decrypt(string $hash): ?string
    {
        $str = base64_decode($hash);
        $iv = substr($str, 0, $this->ivLength);
        $mac = substr($str, $this->ivLength, self::SHA2_LENGTH);
        $raw = substr($str, $this->ivLength + self::SHA2_LENGTH);

        $data = openssl_decrypt($raw, self::METHOD, self::KEY, OPENSSL_RAW_DATA, $iv);
        $check = hash_hmac('sha256', $raw, self::KEY, true);

        return hash_equals($mac, $check) ? $data : null;
    }
}
