<?php declare(strict_types=1);


namespace Core\Helpers;

/**
 * Class Http
 *
 * @package Core\Helpers
 */
class Http
{
    /**
     * Redirect to HTTPS site version
     */
    public static function checkProtocolAndRedirect(): void
    {
        if (!filter_input(INPUT_SERVER, 'HTTPS', FILTER_SANITIZE_STRING)) {
            // redirect to https version
            // exit;
        }
    }

    /**
     * @return bool
     */
    public static function isAjaxRequest(): bool
    {
        $flags = FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH;

        $req = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH', FILTER_SANITIZE_STRING, $flags);

        return ($req === 'XMLHttpRequest');
    }

    /**
     * Show 404
     */
    public static function show404(): void
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        echo '404 Not Found :(';

        exit;
    }
}
