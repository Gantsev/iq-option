<?php declare(strict_types=1);

namespace Core\Helpers;

use Core\Base\Exception\PrivateException;

/**
 * Class Config
 *
 * @package Core\Helpers
 */
class Config
{
    /** Имя файла с конфигом */
    private const FILENAME = 'config.ini';

    /**
     * @var array
     */
    private static $config = [];

    /**
     * Config constructor.
     *
     * @throws \Core\Base\Exception\PrivateException
     */
    public function __construct()
    {
        $filename = PROJECT_PATH . '/' . self::FILENAME;

        if (empty($config)) {
            if (file_exists($filename) && ($config = parse_ini_file($filename, true, INI_SCANNER_TYPED))) {
                self::$config = $config;
            } else {
                throw new PrivateException('Config file not found or parse error!');
            }
        }
    }

    /**
     * Получает значение из конфига
     *
     * @use Config::get('section.variable')
     *
     * @param string $section
     *
     * @throws \Core\Base\Exception\PrivateException
     *
     * @return mixed|null
     */
    public function get(string $section)
    {
        [$path, $name] = explode('.', $section);

        if (!isset(self::$config[$path][$name])) {
            throw new PrivateException('Config section [' . $section . '] not found');
        }

        return self::$config[$path][$name];
    }
}
