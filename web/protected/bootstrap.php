<?php declare(strict_types=1);

define('PROJECT_PATH', __DIR__);
define('APP_PATH', PROJECT_PATH . '/App');
define('WWW_PATH', realpath(PROJECT_PATH . '/../public'));

require_once 'vendor/autoload.php';

set_exception_handler(function (Throwable $exception) {
    switch (true) {
        case $exception instanceof \Core\Base\Exception\PrivateException:
            // Например запись в лог или отправка куда-нибдудь...
            echo $exception->getMessage();
            break;
        case $exception instanceof \Core\Base\Exception\PublicException:
            // Красивая страница какая-нибудь
            echo $exception->getMessage();
            break;
        default:
            // Что-то куда-то зачем-то
            echo $exception->getMessage();
    }
});
