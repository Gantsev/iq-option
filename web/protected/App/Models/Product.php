<?php declare(strict_types=1);

namespace App\Models;

use Core\Base\AbstractModel;
use Core\Base\Interfaces\CacheStorageInterface;
use Core\Base\Interfaces\PersistentStorageInterface;

/**
 * Class Product
 *
 * @package App\Models
 */
class Product extends AbstractModel
{
    /** Количество товаров на главной странице */
    private const RAND_PAGE_COUNT = 3;

    private const RAND_REDIS_KEY = 'product:randomItems';

    private const RAND_REDIS_COUNT = 100;

    private const RAND_REDIS_TTL = 3600 * 3; // 3 Hours

    private const TABLE = 'products';

    /** @var PersistentStorageInterface */
    private $persistent;

    /** @var CacheStorageInterface */
    private $cache;

    public function __construct(PersistentStorageInterface $databaseStorage, CacheStorageInterface $cacheStorage)
    {
        $this->persistent = $databaseStorage;
        $this->cache = $cacheStorage;
    }

    /**
     * @return array
     */
    public function getRandomItems(): array
    {
        $randomItems = $this->getRandomFromCache();

        if (!$randomItems) {
            $this->getRandomItemsFromDatabaseAndPushToRedis();
            $randomItems = $this->getRandomFromCache();
        }

        return (array)$randomItems;
    }

    /**
     * @return array
     */
    private function getRandomFromCache(): array
    {
        return (array)$this->cache->getRandomItems(self::RAND_REDIS_KEY, self::RAND_PAGE_COUNT);
    }

    private function getRandomItemsFromDatabaseAndPushToRedis(): void
    {
        $result = $this->persistent->pdo()->query(
        // Я знаю, что RANDOM() медленный :(
            sprintf(
                'SELECT "id", "title", "price" FROM "%s" ORDER BY RANDOM() LIMIT %d',
                self::TABLE,
                self::RAND_REDIS_COUNT
            ),
            \PDO::FETCH_ASSOC
        );

        while ($row = $result->fetch()) {
            $this->cache->pushRandomItem(self::RAND_REDIS_KEY, $row);
        }

        $this->cache->setExpire(self::RAND_REDIS_KEY, self::RAND_REDIS_TTL);
    }
}
