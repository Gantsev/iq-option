<?php declare(strict_types=1);

namespace App\Controllers;

use Core\Base\AbstractController;

class Basket extends AbstractController
{
    public function my(): void
    {

    }

    public function edit(): void
    {

    }

    public function save(): void
    {

    }

    public function ajaxGetShortInfo(): string
    {

    }

    public function ajaxDeleteItem(int $itemId): string
    {

    }

    public function ajaxAddItem(int $itemId): string
    {

    }
}
