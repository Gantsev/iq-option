<?php declare(strict_types=1);

namespace App\Controllers;

use App\Models\Product;
use Core\Base\AbstractController;
use Core\Base\Resources\PostgresResource;
use Core\Base\Resources\RedisResource;
use Core\Base\Storage\DatabaseStorage;
use Core\Base\Storage\RedisStorage;
use Core\Helpers\Config;

class Main extends AbstractController
{
    private $model;

    public function __construct()
    {
        $this->model = new Product(
            new DatabaseStorage(new PostgresResource(new Config)),
            new RedisStorage(new RedisResource)
        );
    }

    public function index(): void
    {
        echo $this->view('index', [
            'items' => $this->model->getRandomItems(),
        ]);
    }
}
