#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if ! docker ps -q &> /dev/null
then
    echo "You must be in docker group or root"
    exit 1
fi

cd ${DIR}

docker-compose up --force-recreate --build -d nginx
docker-compose run --rm -u "www-data" -w "/var/www/web/protected" php-fpm /usr/bin/composer install
docker-compose run --rm -u "www-data" -w "/var/www/web/protected" php-fpm ./init
